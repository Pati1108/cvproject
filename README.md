# CV Project

## General info
This project contains my CV created for practice HTML and CSS.

## Screenshot
![Example screenshot](./img/screenshoot.png)

## Technologies
* HTML
* CSS

## Status
Project in progress, I am still working to add all my skills.
If You have any advice and ideas don't hesitate to contact me.

## Credits
Project inspired by flynerd.pl

## Where You can find me
[My blog about dietetics](https://healthyfitplace.blogspot.com)

[My blog on Instagram](https://www.instagram.com/healthyfitplace/)

[My blog on Facebook](https://www.facebook.com/HealthyFitPlaceblog/)

[My LinkedIn profile](linkedin.com/in/patrycja-surmela/)

Email: _patrycjasurmela@wp.pl_



